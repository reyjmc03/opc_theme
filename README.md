# Owasp Appsec Conference 2016 Web Template for Drupal

###### Features Updates
- Single Page Application Website

###### Known Bugs
- White screen error on certain installation (updates), no admin menu
- Access key missing node error

###### CHANGE LOGS
***25 November 2015 07:55***
- updated themes in page.tpl.php and alter.css

***25 November 2015 09:44***
- adjusting header logo to a responsive resolution in alter.css

***26 November 2015 00:36***
- adding "about" navigation
- re ajusting header logo for responsiveness

***26 November 2015 11:11***
- modifying README.md

***26 November 2015 11:17***
- modifying README.md again

***27 November 2015 14:39***
- fixing header navigation

***29 November 2015 09:13***
- fixing theme-settings.php
- adding image upload in animate slideshow

